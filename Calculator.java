/* Класс Калькулятор */

public class Calculator {
	
	/** Приватные ментоды и переменные **/
	
	/* Результат вычисления */
	private int result;
	
	
	/** Публичные методы **/
	
	/* Сумма */
	public void add(int ... params) {
		for (Integer param: params) {
			this.result += param;
		}
	}

	/* Разность (2 аргумента)*/
	public void subtract(int ... params) {
		this.result = params[0] - params[1];
	}

	/* Умножение */
	public void multiply(int ... params) {
		if (params[0] != 0) this.result = 1; // Если первый параметр не 0, то присваиваем результату 1, чтобы конечный результат умножения не получился 0
		for (Integer param: params) {
			this.result *= param;
		}
	}

	/* Деление (2 аргумента)*/
	public void devision(int ... params) {
		this.result = params[0] / params[1];
	}
	
	/* Получить результат */
	public int getResult() {
		return this.result;
	}
	
	/* Очистить результат */
	public void cleanResult() {
		this.result = 0;
	}
}