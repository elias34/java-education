import java.util.Scanner;

/*** Пользовательский ввод для Калькулятора ***/
public class InteractRunner {
	public static void main(String[] arg) {
		Scanner reader = new Scanner(System.in);
		try {
			Calculator calc = new Calculator();
			String exit = "n";
			do {
				/* Очистка результата, если ползователь выбрал параметр (R)estart */
				if (exit.equals("r"))
					calc.cleanResult();

				/* Запрос значений у пользователя */
				System.out.println("Enter first arg: ");
				String first = reader.next();
				System.out.println("Enter second arg: ");
				String second = reader.next();
				System.out.println("Enter operation ( + - * / ^): ");
				String operation = reader.next();
				
				/** Передача параметров объекту Калькулятор **/
				
				/* Выбор операции */
				switch (operation)  {
				  case "+":
					calc.add(Integer.valueOf(first), Integer.valueOf(second));
					break;
				  case "-":
					calc.subtract(Integer.valueOf(first), Integer.valueOf(second));
					break;
				  case "*":
					calc.multiply(Integer.valueOf(first), Integer.valueOf(second));
					break;
				  case "/":
					calc.devision(Integer.valueOf(first), Integer.valueOf(second));
					break;
				}
				
				/* Вывод результата и запрос на продолжение */
				System.out.println("Result: " + calc.getResult());
				System.out.println("Continue? (Y)es / (E)xit / (R)estart): ");
				exit = reader.next();
			} while (exit.equals("y") || exit.equals("r"));
		} finally {
			reader.close();
		}
	}
}
